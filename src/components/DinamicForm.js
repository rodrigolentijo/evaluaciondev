import React, { Component } from 'react';
import Input from './Input'

class DinamicForm extends Component{
    constructor(){
        super();
        this.state = {
          cantidad : ''
        };

        this.handleInput = this.handleInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

      }

      handleInput(e){
        const { value, name } = e.target;
        this.setState({
            [name] : value
        })  
        }


        handleSubmit(e){
            e.preventDefault();
        }
    
        createForm(e){
          if (e>10){
            alert("No puede ingresar mas de 10 palabras")            
          }else{
            let form = []  

            for (let i = 0; i < e; i++) {             
              form.push( <Input key = {i}/>)
            }
            return form          

          }            
         }

        render(){
            return(
            <div>
              <div className="row  mt-4">
                 <div className="col-5">
                    <h4>Ingrese la cantidad de palabras a computar</h4>                                  
                 </div>

                 <div className="col-2">
                  <input className="form-control" type="number" onChange={this.handleInput} name="cantidad" placeholder="De 1 a 10"></input>
                 </div>

               </div>
                                               

                {this.createForm(this.state.cantidad)}

              </div>
                                
        )
      }
      
}
export default DinamicForm;

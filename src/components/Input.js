import React, { Component } from 'react';

class Input extends Component {
    constructor(){
        super();
        this.state = {
            palabra: '',
            longitud: ''
                        
        };
        this.handleInput = this.handleInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.similarity = this.similarity.bind(this);

    }

    handleInput(e){
        const { value, name } = e.target;
        this.setState({
            [name] : value
        })
              

        console.log(this.state,"longitud: ", this.state.palabra.length);      
    }

    handleSubmit(e){
        e.preventDefault();
    }



    similarity(e){
        if (e.length>100000){
            alert("longitud de la palabra excedida")
        } else{
            var acum = 0;
            var i;
            console.log("aca");

            for (i=0; i<e.length; i++){
                acum = acum + this.compare(e, i);       
            }
            return(acum)
            }
        
        }

    compare(e,i) {
        var cont = 0;
            var origin = 0;
            var sub =i;
            console.log("aca tabien");

            while ((e.length - sub) >0){
                if (e[origin] == e[sub]){
                    cont ++;
                    sub ++;
                    origin ++;
                }else{
                    return(cont)
                }
            }
            return(cont)
        }


  

    
    render(){
        return(
            <form onSubmit={this.handleSubmit}>
            
              <div className="row ml-4 mt-4">
                  <div className="col-6">
                        <span>Ingrese una palabra a computar</span>
                        <input type="text" id={this.key} onChange={this.handleInput} name="palabra" placeholder="Ingrese palabra" className="form-control"></input>
                  </div>
                  <div className="col-2">                   
                    <span>Resultado</span>
                    <h1>{this.similarity(this.state.palabra)}</h1>
                  </div>
              </div>
                                    
          </form>

        )
    }
}

export default Input;
import React, { Component } from 'react';
import './App.css';
import DinamicForm from './components/DinamicForm';
import Navigation from './components/Navigation.js';




function App() {

      return(
          <div className="App">
            
            <Navigation titulo="Evaluación Devlights - Lentijo Rodrigo"/>
            <DinamicForm />  
               
          </div>
        )

  }

export default App;
